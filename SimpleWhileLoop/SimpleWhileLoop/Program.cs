﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 10;
            var i = 0;

            while (i < counter)
            {
                var output = i + 1;
                Console.WriteLine($"This is line {output}");
                i++;
            }
        }
    }
}
